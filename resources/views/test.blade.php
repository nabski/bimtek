<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	Hello @yield('name', 'nama')
	@php
	$var = 'kamis';
	$hari = ['senin', 'selasa', 'rabu', 'kamis'];
	@endphp
	{{ $var }}

	<br>

	@if($var == 'kamis')
	Hari ini hari {{ $var }}
	@else
	Libur
	@endif

	@isset($var)
	Var isset.
	@endisset

	@foreach($hari as $day)
	{{ $day }} <br>
	@endforeach
	<br>
	<br>
	
	@guest
		<button> Login </button>
	@endguest
	
	<br>
	<br>

	@auth
		<button> Logout </button>

	@endauth

</body>
</html>