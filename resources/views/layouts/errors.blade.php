@if ($errors->any())
<div class="alert alert-danger">
	<button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
	<i class="pci-cross pci-circle"></i>
	@foreach($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
</div>
@endif