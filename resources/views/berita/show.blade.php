@extends('layouts.app')
@section('title','Berita')
@section('breadcrumb_1','Berita')
@section('breadcrumb_2','Detail')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="panel blog blog-details">
			<div class="panel-body">
				<div class="blog-title media-block">
					<div class="media-right textright">
						<a href="#" class="btn btn-icon demo-psi-twitter icon-lg add-tooltip" data-original-title="Twitter" data-container="body"></a>
						<a href="#" class="btn btn-icon demo-psi-instagram icon-lg add-tooltip" data-original-title="Instagram" data-container="body"></a>
					</div>
					<div class="media-body">
						<a href="#" class="btn-link">
							<h1>{{ $berita->judul }}</h1>
						</a>
						{{-- <p>By <a href="#" class="btn-link">Aaron Chavez</a></p> --}}
					</div>
				</div>
				<div class="blog-content">
					
					<div class="blog-body">
						{{ $berita->konten }}
					</div>
				</div>
				<div class="blog-footer">
					<div class="media-left">
						<span class="label label-success">{{ $berita->created_at->diffForHumans() }}</span>
						<small>Posted by : <a href="#" class="btn-link">?</a></small>
					</div>
					<div class="media-body text-right">
						<span class="mar-rgt"><i class="demo-pli-heart-2 icon-fw"></i>519</span>
						<i class="demo-pli-speech-bubble-5 icon-fw"></i>23
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection	