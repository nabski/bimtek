@extends('layouts.app')
@section('title','Berita')
@section('breadcrumb_1','Berita')
@section('breadcrumb_2','Index')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="panel panel-body">
			<div class="panel-body">
                <table id="dt" class="table table-hover table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Judul</th>
                            <th>Tanggal</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        @foreach($berita as $b)
                        <tr id="{{ 'row'.$b->id }}">
                            <td>{{ $no++ }}</td>
                            <td>{{ $b->judul }}</td>
                            <td>{{ $b->created_at ? $b->created_at->format('j M Y') : null }}</td>
                            <td>
                                <form action="{{ url('/berita', ['id' => $b->id]) }}" method="post" id="delete-form">
                                    <div class="btn-group  btn-group-sm">
                                        <a class="btn btn-info" href="{{ url('berita/'.$b->id) }}" title="">Detail</a>
                                        <a href="{{ url('berita/'.$b->id.'/edit') }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Ubah">Edit</a>
                                            {!! method_field('delete') !!}
                                            {!! csrf_field() !!}
                                            <button class="btn btn-danger" onclick="return confirm('Delete this data?')">Delete</button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>  
                </table>
                //mencetak paginator
                {{ $berita->links() }}
            </div>
        </div>
    </div>
</div>
@endsection	
