{{ csrf_field() }}
<input type="text" placeholder="Judul" name="judul" class="form-control" value="{{ isset($berita->judul) ? $berita->judul : null }}">
<textarea id="summernote" name="konten">{{ isset($berita->konten) ? $berita->konten : null }}</textarea>
<button type="submit" class="btn btn-success">Submit</button>
<button type="reset" class="btn btn-default">Reset</button>