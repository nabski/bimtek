@extends('layouts.app')
@section('title','Berita')
@section('breadcrumb_1','Berita')
@section('breadcrumb_2','Tambah Berita')

@section('content')
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="panel panel-body">
			<div class="panel-body">
				<form action="{{ route('berita.store') }}" method="POST" accept-charset="utf-8">
					@include('berita.form')
				</form>
			</div>
		</div>
	</div>
</div>
@endsection	