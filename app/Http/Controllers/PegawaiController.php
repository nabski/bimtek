<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;

class PegawaiController extends Controller
{
    # GET localhost:8000/
	public function index() {
		# Tarik semua isi tabel pegawai kedalam variabel
		$pegawai = Pegawai::all();

		# Tampilkan View
		return view('pegawai.index', compact('pegawai'));
	}

	# GET localhost:8000/buat
	public function baru() {
		# Buat dropdown jenis kelamin
		$jenis_kelamin = [
			'L' => 'Laki-laki', 
			'P' => 'Perempuan'
		];

		# Tampilkan halaman pembuatan pegawai
		return view('pegawai.buat', compact('jenis_kelamin'));
	}

	# POST localhost:8000/buat
	public function buat(Request $request) {
		
		# Buat aturan validasi
		$aturan = array(
			'nama' => 'required|min:3', 
			'jabatan' => 'required', 
			'usia' => 'required', 
			'telepon' => 'required', 
			'email' => 'required|email'
		);	

		# Buat pesan error validasi manual
		$pesan = array(
			'nama.required' => 'Nama wajib diisi.',
			'nama.min' => 'Inputan minimal 3 karakter.',
			'jabatan.required' => 'Jabatan wajib diisi.',
			'usia.required' => 'Inputan Usia wajib diisi.',
			'telepon.required' => 'Telepon wajib diisi.',
			'email.required' => 'Email wajib diisi.',
			'email.email' => 'Inputan harus berupa Email.'
		);

		# Validasi
		$validasi = validator()->make($request->all(), $aturan, $pesan);

		# Bila validasi gagal
		if($validasi->fails()) {

			# Kembali kehalaman sama dengan pesan error
			return redirect()->back()
				->withErrors($validasi)->withInput();

		# Bila validasi sukses
		} else {

			# Simpan kedalam database
			Pegawai::create([
				'nama' => $request->nama, 
				'jabatan' => $request->jabatan, 
				'usia' => $request->usia, 
				'jenis_kelamin' => $request->jenis_kelamin, 
				'telepon' => $request->telepon, 
				'email' => $request->email
			]);

			# Kehalaman beranda dengan pesan sukses
			return redirect()->route('beranda')
				->withPesan('Pegawai baru berhasil ditambahkan.');
		}
	}

	# GET localhost:8000/lhat/{id}
	public function lihat($id) {
		# Ambil data dalam berdasarkan berdasarkan id
		$pegawai = Pegawai::find($id);

		# Tampilkan view
		return view('pegawai.lihat', compact('pegawai'));
	}

	# GET localhost:8000/ubah/{id}
	public function ubah($id) {
		# Buat dropdown jenis kelamin
		$jenis_kelamin = [
			'L' => 'Laki-laki', 
			'P' => 'Perempuan'
		];

		# Tentukan pegawai yang ingin diubah berdasarkan id
		$pegawai = Pegawai::find($id);

		# Tampilkan view
		return view('pegawai.ubah', compact('jenis_kelamin', 'pegawai'));
	}

	# PUT localhost:8000/ubah/{id}
	public function ganti(Request $request, $id) {
		# Buat aturan validasi
		$aturan = array(
			'nama' => 'required|min:3', 
			'jabatan' => 'required', 
			'usia' => 'required', 
			'telepon' => 'required', 
			'email' => 'required|email'
		);	

		# Buat pesan error validasi manual
		$pesan = array(
			'nama.required' => 'Nama wajib diisi.',
			'nama.min' => 'Inputan minimal 3 karakter.',
			'jabatan.required' => 'Jabatan wajib diisi.',
			'usia.required' => 'Inputan Usia wajib diisi.',
			'telepon.required' => 'Telepon wajib diisi.',
			'email.required' => 'Email wajib diisi.',
			'email.email' => 'Inputan harus berupa Email.'
		);

		# Validasi
		$validasi = validator()->make($request->all(), $aturan, $pesan);

		# Bila validasi gagal
		if($validasi->fails()) {

			# Kembali kehalaman sama dengan pesan error
			return redirect()->back()->withErrors($validasi)->withInput();

		# Bila validasi sukses
		} else {

			# Update isi database
			Pegawai::find($id)->update([
				'nama' => $request->nama, 
				'jabatan' => $request->jabatan, 
				'usia' => $request->usia, 
				'jenis_kelamin' => $request->jenis_kelamin, 
				'telepon' => $request->telepon, 
				'email' => $request->email
			]);

			# Kehalaman beranda dengan pesan sukses
			return redirect()->route('beranda')
				->withPesan('Data pegawai berhasil diubah.');
		}
	}

	# DELETE localhost:8000/hapus/{id}
	public function hapus($id) {
		# Hapus biodata berdasarkan id
		Pegawai::find($id)->delete();

		# Kembali kehalaman yang sama dengan pesan sukses
		return redirect()->back()->withPesan('Pegawai berhasil dihapus.');
	}
}
