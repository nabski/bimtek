<?php

namespace App\Http\Controllers;

//jangan lupa untuk menambahkan model sebelum modelnya dipakai
use App\Berita;
use Illuminate\Http\Request;

class BeritaController extends Controller
{
    

    public function index()
    {
        // ambil semua data berita
        // $berita = Berita::all();

        // ambil semua data berita dengan paginasi tiap 20 item
        $berita = Berita::paginate(20);
        return view('berita.index', compact('berita'));
    }

    

    public function create()
    {
        return view('berita.create');
    }

   

    public function store(Request $request)
    {
        //validasi 
        $this->validate($request, ['judul'=>'required','konten'=>'required']);

        // jika name di form sama dengan nama atribut
        $berita = Berita::create($request->all());

        // jika name di form beda dengan nama atribut atau mau modifikasi data sebelum disave
        // $berita = new Berita;
        // $berita->judul = $request->judul;
        // $berita->konten = $request->konten;
        // $berita->save();
        
        //redirect ke show berita yang sudah diedit
        return redirect('berita/'.$berita->id);
    }

    

    public function show($id)
    {
        $berita = Berita::findOrFail($id);
        return view('berita.show', compact('berita'));
    }

    

    public function edit($id)
    {
        $berita = Berita::findOrFail($id);
        return view('berita.edit', compact('berita'));
    }



    public function update(Request $request, $id)
    {
        $this->validate($request, ['judul'=>'required','konten'=>'required']);
        $berita = Berita::findOrFail($id);
        $berita->update($request->all());

        //redirect ke show berita yang sudah diedit
        return redirect('berita/'.$berita->id);
    }

   

    public function destroy($id)
    {
        $berita = Berita::findOrFail($id);
        $berita->delete();
        return redirect('berita');
        
    }
}
