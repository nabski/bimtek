<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BiodataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('biodata')->insert([
            'nama'       	=> 'Noviyanto Rahmadi',
            'jenis_kelamin'=> 'L',
            'telepon'      => '08115555573',
            'email'       	=> 'novay@enterwind.com',
            'profil'    	=> 'Lorem ipsum dolor sit amet.',
            'created_at' 	=> Carbon::now(),
            'updated_at' 	=> Carbon::now(),
        ]);
    }
}