<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PalsuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('id_ID');
        foreach (range(1, 100) as $loop) {
            DB::table('biodata')->insert([
                'nama'          => $faker->name,
	            'jenis_kelamin' => $faker->randomElement(['L' ,'P']),
	            'telepon'       => $faker->phoneNumber,
	            'email'         => $faker->email,
	            'profil'        => $faker->text,
	            'created_at'    => Carbon::now(),
	            'updated_at'    => Carbon::now(),
            ]);
        }
    }
}