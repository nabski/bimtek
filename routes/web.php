<?php

Route::get('/', function () {
    return view('layouts.app');
});

Route::resource('berita', 'BeritaController');






Route::get('/o', function(){
	$test = \Faker\Factory::create();
	for ($i=0; $i < 50 ; $i++) { 
		$p = ['judul'=>$test->sentence,'konten'=>$test->paragraph];
		$new = DB::table('beritas')->insert($p);
	}
});

















// Route::get('/home', 'BiodataController@index');

// # Halaman muka, untuk menampilkan semua data pegawai yang ada. 
// # localhost:8000
// Route::get('/', [
// 	'as' => 'beranda', 
// 	'uses' => 'PegawaiController@index'
// ]);

// # Halaman yang berisi Form inputan Pegawai baru 
// # localhost:8000/buat
// Route::get('buat', [
// 	'as' => 'baru', 'uses' => 'PegawaiController@baru'
// ]);

// # Memproses Form lalu mengirimnya kedalam database 
// # localhost:8000/buat
// Route::post('buat', [
// 	'as' => 'buat', 'uses' => 'PegawaiController@buat'
// ]);

// # Menampilkan pegawai perorangan 
// # URL: localhost:8000/lihat/{id}
// Route::get('lihat/{id}', [
// 	'as' => 'lihat', 'uses' => 'PegawaiController@lihat'
// ]);

// # Form untuk mengubah isi Pegawai dalam database 
// # localhost:8000/ubah/{id}
// Route::get('ubah/{id}', [
// 	'as' => 'ubah', 'uses' => 'PegawaiController@ubah'
// ]);

// # Memproses Form lalu mengirim yang baru kedalam database 
// # localhost:8000/ubah/{id}
// Route::put('ubah/{id}', [
// 	'as' => 'ganti', 'uses' => 'PegawaiController@ganti'
// ]);

// # Tindakan untuk menghapus pegawai 
// # URL: localhost:8000/{id}/hapus
// Route::delete('hapus/{id}', [
// 	'as' => 'hapus', 'uses' => 'PegawaiController@hapus'
// ]);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
